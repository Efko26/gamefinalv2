package game;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;

public class GameFrame extends Frame {
	public GameMapPanel mapPanel;
	Map map;
	Label label;
	TextField textField;
	TextArea textArea;
	Button button,button1;	
	
	GameFrame(Map m) {
		map = m;
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		
		label = new Label();
		label.setLocation(250, 50);
		label.setSize(250, 50);
		label.setText("Входа на Любимото–МГ");
		this.add(label);
		
		textArea = new TextArea();
		textArea.setLocation(250, 170);
		textArea.setSize(225, 150);
		textArea.setText("Цъкнете върху Червеното");
		textArea.setEnabled(false);
		this.add(textArea);
	
		button = new Button();
		button.setLabel("Нека да влизаме");
		button.setLocation(50, 250);
		button.setBackground(Color.YELLOW);
		button.setSize(150, 30);
		GameActionListener gal = new GameActionListener(this);
		button.addActionListener(gal);
		this.add(button);
		
		button1 = new Button("Пробвай се да избягаш");
		button1.setLocation(1100, 550);
		button1.setSize(300,200);
		button1.setBackground(Color.red);
		GameActionListener gal1 = new GameActionListener(this);
		button1.addActionListener(gal1);
		this.add(button1);
		
		mapPanel = new GameMapPanel(map);
		mapPanel.setLocation(50, 50);
		mapPanel.setSize(map.sizex * 30 + 1, map.sizey * 30 + 1);
		GameKeyListener gkl = new GameKeyListener(this);
		mapPanel.addKeyListener(gkl);
		GameMouseListener gml = new GameMouseListener(this);
		mapPanel.addMouseListener(gml);
		
		
		this.setLayout(null);
	}
}
