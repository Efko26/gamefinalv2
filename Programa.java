package game;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Programa {
	public static void main(String[] args) {
		Map m;
		
		try {
			FileInputStream fis = new FileInputStream("map.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
		m = (Map) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			m = new Map(12,7);
			Hero h = new Hero(0, 2, m);
			m.hero = h;
		
		}
	
			

		GameFrame f = new GameFrame(m);
		f.setSize(500, 500);
		f.setVisible(true);
		
		Scanner scan = new Scanner(System.in);
		while (true) {
			char c = scan.next().charAt(0);
			
			m.hero.move(c);
			
		}
	}
}