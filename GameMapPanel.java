package game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;

public class GameMapPanel extends Panel {
	Map map;
	
      	GameMapPanel(Map m) { 
		map = m;
	}
	
	public void paint(Graphics g) {
		for (int y = -3; y<3 ; y++) {
			for (int x = -3; x < 3; x++) {
				g.setColor(Color.black);
				if (y == 0 && x == 0 ) {
					g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
				} else {
					g.drawRect((x+2) * 30, (y+2) * 30, 30, 30);
				} 
				if (map.hero.posx + x == 1 && map.hero.posy + y == 2) {
					g.setColor(Color.green);
					g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
                  			
				}if (map.hero.posx + x == 5 && map.hero.posy + y == 4) {
					g.setColor(Color.red);
					g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
				}if (map.hero.posx + x == 7 && map.hero.posy + y == 3) {
					g.setColor(Color.PINK);
					g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
				}if (map.hero.posx + x == 11 && map.hero.posy + y == 4) {
					g.setColor(Color.DARK_GRAY);
					g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
				}if (map.hero.posx + x == 9 && map.hero.posy + y == 4) {
					g.setColor(Color.BLUE);
						g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
				}
					if (map.hero.posx + x < 0 || map.hero.posy + y < 0 || map.hero.posx + x >= map.sizex || map.hero.posy + y >= map.sizey ) {
					g.setColor(Color.red);
					g.fillRect((x+2) * 30, (y+2) * 30, 30, 30);
				}
			}	
	
		}
	}
	
}