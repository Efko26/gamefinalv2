package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {
	GameFrame frame;
	
	GameKeyListener(GameFrame f) {
		frame = f;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		System.out.println(c);
		frame.map.hero.move(c);
		frame.mapPanel.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int a = e.getKeyCode();
		char c;
		switch (a) {
			case 37: c = 'a'; break;
			case 38: c = 'w'; break;
			case 39: c = 'd'; break;
			case 40: c = 's'; break;
			default: c = '/';
		}
		frame.map.hero.move(c);
		frame.mapPanel.repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}